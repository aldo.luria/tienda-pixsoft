from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "tienda_pixsoft.users"
    verbose_name = _("Users")