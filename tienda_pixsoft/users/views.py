from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, ListView, RedirectView, UpdateView, TemplateView, CreateView, DeleteView
from django.contrib.auth.views import LoginView, LogoutView
from django.http import HttpResponseRedirect
from django.db.models import Q, Max, Min
from tienda_pixsoft.productos.models import Producto, Comentario, CarritoCompras, Categoria, Marca, Sucursal, ImagenesProducto
from .forms import UserCreationFormWithEmail, EmailForm
from django import forms

User = get_user_model()

class Indice(TemplateView):
    template_name = 'index.html'

# Views de usuarios
class Ingresar(LoginView):
	template_name = 'cuenta/login.html'

	def get(self, request, *args, **kwargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect(reverse('indice'))
		else:
			context = self.get_context_data(**kwargs)
			return self.render_to_response(context)

	def get_success_url(self):
		return reverse_lazy('indice')

class Salir(LogoutView):
    next_page = reverse_lazy('indice')

class Registrar(CreateView):
    form_class = UserCreationFormWithEmail
    template_name = 'cuenta/signup.html'

    def get_success_url(self):
	    return "/ingresar/{}".format('?registrado')

class CambiarPerfil(LoginRequiredMixin, UpdateView):
    model = User
    fields = ('first_name','last_name','telefono','pais','estado','municipio','localidad','codigoPostal','calle','numeroInt','numeroExt')
    success_url = '/'
    template_name = 'usuario/perfil.html'
    login_url = 'ingresar'

    def get_object(self, queryset=None):
        return self.request.user

class CambiarEmail(LoginRequiredMixin, UpdateView):
    form_class = EmailForm
    success_url = reverse_lazy('cambiar_perfil')
    template_name = 'usuario/email.html'

    def get_object(self):
        return self.request.user

# Views de categoría
class CrearCategoria(PermissionRequiredMixin, CreateView):
    model = Categoria
    template_name = 'categoria/crear_categoria.html'
    fields = {'nombreCategoria', 'descripcionCategoria',}
    success_url = reverse_lazy('listar_categoria')
    login_url = 'ingresar'
    permission_required = 'productos.add_categoria'

class ListarCategoria(PermissionRequiredMixin, ListView):
    model = Categoria
    template_name = 'categoria/listar_categorias.html'
    login_url = 'ingresar'
    permission_required = 'productos.view_categoria'
    paginate_by = 24

class EditarCategoria(PermissionRequiredMixin, UpdateView):
    model = Categoria
    template_name = 'categoria/editar_categoria.html'
    fields = {'nombreCategoria', 'descripcionCategoria',}
    success_url = reverse_lazy('listar_categoria')
    login_url = 'ingresar'
    permission_required = 'productos.change_categoria'

class EliminarCategoria(PermissionRequiredMixin, DeleteView):
    model = Categoria
    success_url = reverse_lazy('listar_categoria')
    login_url = 'ingresar'
    permission_required = 'productos.delete_categoria'

# Views de marca
class CrearMarca(PermissionRequiredMixin, CreateView):
    model = Marca
    template_name = 'marca/crear_marca.html'
    fields = {'nombreMarca', 'descripcionMarca',}
    success_url = reverse_lazy('listar_marca')
    login_url = 'ingresar'
    permission_required = 'productos.add_marca'

class ListarMarca(PermissionRequiredMixin, ListView):
    model = Marca
    template_name = 'marca/listar_marca.html'
    login_url = 'ingresar'
    permission_required = 'productos.view_marca'
    paginate_by = 24

class EditarMarca(PermissionRequiredMixin, UpdateView):
    model = Marca
    template_name = 'marca/editar_marca.html'
    fields = {'nombreMarca', 'descripcionMarca',}
    success_url = reverse_lazy('listar_marca')
    login_url = 'ingresar'
    permission_required = 'productos.change_marca'

class EliminarMarca(PermissionRequiredMixin, DeleteView):
    model = Marca
    success_url = reverse_lazy('listar_marca')
    login_url = 'ingresar'
    permission_required = 'productos.delete_marca'

# Views de sucursal
class CrearSucursal(PermissionRequiredMixin, CreateView):
    model = Sucursal
    template_name = 'sucursal/crear_sucursal.html'
    fields = {'nombreSucursal', 'descripcionSucursal',}
    success_url = reverse_lazy('listar_sucursal')
    login_url = 'ingresar'
    permission_required = 'productos.add_sucursal'

class ListarSucursal(PermissionRequiredMixin, ListView):
    model = Sucursal
    template_name = 'sucursal/listar_sucursal.html'
    login_url = 'ingresar'
    permission_required = 'productos.view_sucursal'
    paginate_by = 24

class EditarSucursal(PermissionRequiredMixin, UpdateView):
    model = Sucursal
    template_name = 'sucursal/editar_sucursal.html'
    fields = {'nombreSucursal', 'descripcionSucursal', 'disponible',}
    success_url = reverse_lazy('listar_sucursal')
    login_url = 'ingresar'
    permission_required = 'productos.change_sucursal'

class EliminarSucursal(PermissionRequiredMixin, DeleteView):
    model = Sucursal
    success_url = reverse_lazy('listar_sucursal')
    login_url = 'ingresar'
    permission_required = 'productos.delete_sucursal'

# Views de producto
#    fields = {'claveProducto', 'codigoFabricante', 'nombreProducto','descripcionProducto','precio','promos','diaGarantia','mesGarantia','anioGarantia','cantidadProducto','categoria','marca',}

class CrearProducto(PermissionRequiredMixin, CreateView):
    model = Producto
    template_name = 'producto/crear_producto.html'
    fields = {'claveProducto', 'codigoFabricante', 'nombreProducto','descripcionProducto','precio','promos','diaGarantia','mesGarantia','anioGarantia','cantidadProducto','categoria','marca',}
    login_url = 'ingresar'
    permission_required = 'productos.add_producto'

    def get_success_url(self):
	    return "/producto/crear/{}".format('?creado')

class EditarProducto(PermissionRequiredMixin, UpdateView):
    model = Producto
    template_name = 'producto/editar_producto.html'
    fields = {'claveProducto', 'codigoFabricante', 'nombreProducto','descripcionProducto','precio','promos','diaGarantia','mesGarantia','anioGarantia','cantidadProducto','categoria','marca','disponible'}
    login_url = 'ingresar'
    permission_required = 'productos.change_producto'

    def get_success_url(self):
	    return "/producto/detalle/{}/".format(self.object.pk)

class EliminarProducto(PermissionRequiredMixin, DeleteView):
    model = Producto
    success_url = reverse_lazy('listar_producto')
    login_url = 'ingresar'
    permission_required = 'productos.delete_producto'

class ListarProducto(ListView):
    template_name = 'producto/listado_productos.html'
    model = Producto
    paginate_by = 24

    def get_queryset(self):
        query = None

        if ('busca' in self.request.GET) and self.request.GET['busca'] != "":
            query = Q(nombreProducto__icontains=self.request.GET["busca"])

        if query is not None:
            productos = Producto.objects.filter(query,disponible=True)
        else:
            productos = Producto.objects.filter(disponible=True)
        return productos

class DetalleProducto(DetailView):
    template_name = 'producto/detalle.html'
    model = Producto

class ListarProductoAdministrador(PermissionRequiredMixin, ListView):
    template_name = 'producto/listar_producto.html'
    model = Producto
    paginate_by = 24
    permission_required = 'productos.view_producto'

    def get_queryset(self):
        query = None

        if ('busca' in self.request.GET) and self.request.GET['busca'] != "":
            query = Q(nombreProducto__icontains=self.request.GET["busca"])

        if query is not None:
            productos = Producto.objects.filter(query)
        else:
            productos = Producto.objects.all()
        return productos

class ImagenProducto(PermissionRequiredMixin, DetailView):
    template_name = 'producto/imagen_producto.html'
    model = Producto
    permission_required = 'productos.view_producto'

class AgregarImagen(PermissionRequiredMixin, CreateView):
    model = ImagenesProducto
    template_name = 'producto/detalle.html'
    fields = {'imagen', 'producto'}
    login_url = 'ingresar'
    permission_required = 'productos.add_imagenesproducto'

    def get_success_url(self):
	    return "/producto/imagenes/{}/{}".format(self.object.producto.pk,'?agregado')

class EliminarImagen(PermissionRequiredMixin, DeleteView):
    model = ImagenesProducto
    login_url = 'ingresar'
    permission_required = 'productos.delete_imagenesproducto'

    def get_success_url(self):
	    return "/producto/imagenes/{}/{}".format(self.object.producto.pk,'?eliminado')

# Views de comentario
class ComentarioProducto(CreateView):
    template_name = 'producto/detalle.html'
    model = Comentario
    fields = {'comentario', 'nombreUComentario', 'producto',}

    def get_success_url(self):
        return "/producto/detalle/{}/".format(self.object.producto.pk)

# Views de carrito
class AnadirCarrito(LoginRequiredMixin, CreateView):
    model = CarritoCompras
    fields = ('usuario','producto','cantidad','precio','direccion',)
    success_url = reverse_lazy('listado_productos')
    login_url = 'ingresar'

class EliminarCarrito(LoginRequiredMixin,DeleteView):
    queryset = CarritoCompras.objects.filter(comprado=False)
    model = CarritoCompras
    success_url = reverse_lazy('listar_carrito')
    login_url = 'ingresar'

class ListarCarrito(LoginRequiredMixin,ListView):
    template_name = 'carrito/carrito.html'
    model = CarritoCompras
    queryset = CarritoCompras.objects.filter(comprado=False,pendiente=False)
    login_url = 'ingresar'

    def get_context_data(self, **kwargs):
        context = super(ListarCarrito, self).get_context_data(**kwargs)
        context['tab'] = 'sincomprar'
        return context

class ListarCarritoPendientes(LoginRequiredMixin,ListView):
    template_name = 'carrito/carrito.html'
    model = CarritoCompras
    queryset = CarritoCompras.objects.filter(comprado=False,pendiente=True)
    login_url = 'ingresar'

    def get_context_data(self, **kwargs):
        context = super(ListarCarritoPendientes, self).get_context_data(**kwargs)
        context['tab'] = 'pendientes'
        return context

class ListarCarritoFinalizadas(LoginRequiredMixin,ListView):
    template_name = 'carrito/carrito.html'
    model = CarritoCompras
    queryset = CarritoCompras.objects.filter(comprado=True,pendiente=False)
    login_url = 'ingresar'

    def get_context_data(self, **kwargs):
        context = super(ListarCarritoFinalizadas, self).get_context_data(**kwargs)
        context['tab'] = 'finalizadas'
        return context

class ListarVenta(PermissionRequiredMixin, ListView):
    template_name = 'carrito/venta.html'
    model = CarritoCompras
    queryset = CarritoCompras.objects.filter(comprado=True,pendiente=False)
    login_url = 'ingresar'
    permission_required = 'productos.view_carritocompras'
    paginate_by = 24



########## pasarela de pagos #####################
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import hashlib
import requests
import json


class paymentDetail():
    merchantId = 508029
    accountId = 512324
    apiKey = '4Vj8eK4rloUd272L48hsrarnUA'
    description = "compra realizada desde mi sitio"
    test = 1
    url = "https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu"

@method_decorator(csrf_exempt, name='dispatch')
class SummaryView(TemplateView):
    template_name = 'carrito/fin_detalle_compra.html'

    def post(self, request, *args, **kwargs):
        merchand_id          = request.POST['merchant_id']
        reference_sale       = request.POST['reference_sale']
        state_pol            = request.POST['state_pol']
        value                = request.POST['value']
        currency             = request.POST['currency']
        sign                 = request.POST['sign']

        value_str = str(value)

        value_antes, value_despues = value_str.split(".")
        value_despues= list(value_despues)
        if value_despues[1] == '0':
            value= round(float(value),1)
        signature          = hashlib.md5('{}~{}~{}~{}~{}~{}'.format(paymentDetail().apiKey, merchand_id,reference_sale, value, currency,state_pol).encode('utf-8')).hexdigest()

        if signature == request.POST["sign"]:
            carritoModels = CarritoCompras.objects.filter(identificador=reference_sale,comprado=False)
            if state_pol == '4':
                for carrito in carritoModels:
                    carrito.comprado = True
                    carrito.pendiente = False
                    carrito.datos_payu = "{}".format(request.POST)
                    carrito.save()

                if len(carritoModels) != 0:
                    print("compra realizada exitosamente")

            # 104 error 
            # 5 expirada
            # 6 declinada
            elif state_pol == '104' or state_pol == '5' or state_pol == '6':
                carritoModels.delete()
        else:
            print("el signature no coincide")

        return HttpResponse(status=200)



class DetailPaymentView(TemplateView):
    template_name = 'carrito/detalle_compra.html'

    def get_context_data(self, **kwargs):
        payment = paymentDetail()
        description  = payment.description
        merchantId  = payment.merchantId
        accountId  = payment.accountId
        context = super(DetailPaymentView, self).get_context_data(**kwargs)
        referenceCode = ""
        amount = 0
        currency = 'MXN'

        context["merchant_id"] = merchantId
        context["account_id"] = accountId
        context["description"] = description
        context["reference_code"] = referenceCode
        context["amount"] = amount
        context["tax"] = 0
        context["taxReturn_base"] = 0
        context["currency"] = currency
        context["signature"] = ""
        context["test"] = payment.test
        context["buyer_email"] = self.request.user.email
        context["response_url"] = 'https://c0be4f40.ngrok.io/pagar/'
        context["confirmation_url"] = 'https://c0be4f40.ngrok.io/confirmacion/'
        context["url"] = payment.url
        return context


def updateCar(request):
    payment = paymentDetail()
    merchantId  = payment.merchantId
    apiKey = payment.apiKey

    maximo_identificador = CarritoCompras.objects.all().order_by('-identificador')[0].identificador
    maximo_identificador =  maximo_identificador + 1
    currency = 'MXN'
    
    carritos = request.user.carrito_usuario.all().filter(comprado = False, pendiente = False)

    amount = 0
    for carrito in carritos:
        carrito.pendiente = True
        carrito.identificador = maximo_identificador
        carrito.direccion = request.POST["direction"]
        carrito.save()
        amount = amount + carrito.total
    signature = hashlib.md5("{}~{}~{}~{}~{}".format(apiKey,merchantId,maximo_identificador,amount,currency).encode('utf-8') ).hexdigest()

    response = HttpResponse(json.dumps({"precio":amount,"identifier":maximo_identificador,"signature":signature}), content_type="application/json", status=200)
    return response
