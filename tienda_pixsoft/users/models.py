from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    telefono = CharField(max_length=45, null=True)
    pais = CharField(max_length=255, null=True)
    estado = CharField(max_length=255, null=True)
    municipio = CharField(max_length=255, null=True)
    localidad = CharField(max_length=255, null=True)
    codigoPostal = CharField(max_length=45, null=True)
    calle = CharField(max_length=255, null=True)
    numeroInt = CharField(max_length=45, null=True)
    numeroExt = CharField(max_length=45, null=True)
    

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})
