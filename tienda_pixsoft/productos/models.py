from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator

class Categoria(models.Model):
    nombreCategoria = models.CharField(max_length=300)
    descripcionCategoria = models.CharField(max_length=300)
    creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    actualizado = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")


    class Meta:
        verbose_name = "Categoría"
        verbose_name_plural = "Categorías"
        ordering = ['nombreCategoria']

    def __str__(self):
        return self.nombreCategoria

class Marca(models.Model):
    nombreMarca = models.CharField(max_length=300)
    descripcionMarca = models.CharField(max_length=300)
    creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    actualizado = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Marca"
        verbose_name_plural = "Marcas"
        ordering = ['nombreMarca']

    def __str__(self):
        return self.nombreMarca

class Producto(models.Model):
    claveProducto = models.CharField(max_length=45)
    codigoFabricante = models.CharField(max_length=45)
    nombreProducto = models.CharField(max_length=400)
    descripcionProducto = models.TextField()
    precio = models.FloatField()
    promos = models.CharField(max_length=300)
    diaGarantia = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    mesGarantia = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    anioGarantia = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    cantidadProducto = models.IntegerField(default=1, validators=[MinValueValidator(1)])
    categoria = models.ForeignKey(Categoria, related_name="producto_categoria",on_delete=models.CASCADE)
    marca = models.ForeignKey(Marca, related_name="producto_marca",on_delete=models.CASCADE)
    disponible = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    actualizado = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Producto"
        verbose_name_plural = "Productos"
        ordering = ['nombreProducto']

    def __str__(self):
        return self.nombreProducto

class Comentario(models.Model):
    comentario = models.CharField(max_length=300)
    nombreUComentario = models.CharField(max_length=45)
    creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    producto = models.ForeignKey(Producto, related_name="producto_comentarios",on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Comentario"
        verbose_name_plural = "Comentarios"
        ordering = ['-creado']

    def __str__(self):
        return "{} : {} - {}".format(self.comentario, self.producto, self.nombreUComentario)

class ImagenesProducto(models.Model):
    imagen = models.ImageField(upload_to="imagenes_producto")
    creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    producto = models.ForeignKey(Producto, related_name="producto_imagenes",on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Imagen"
        verbose_name_plural = "Imágenes"
        ordering = ['-creado']

    def __str__(self):
        return "{} : {}".format(self.producto.nombreProducto, self.producto)

class Sucursal(models.Model):
    nombreSucursal = models.CharField(max_length=300)
    descripcionSucursal = models.CharField(max_length=300)
    disponible = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")

    class Meta:
        verbose_name = "Sucursal"
        verbose_name_plural = "Sucursales"
        ordering = ['nombreSucursal']

    def __str__(self):
        return self.nombreSucursal

class CarritoCompras(models.Model):
    usuario = models.ForeignKey(get_user_model(), related_name="carrito_usuario",on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, related_name="producto_carrito",on_delete=models.CASCADE)
    precio = models.FloatField()
    cantidad = models.IntegerField(default=1, validators=[MaxValueValidator(100), MinValueValidator(1)])
    total = models.FloatField(validators=[MinValueValidator(0)],null=True)
    direccion  = models.CharField(max_length=300)
    identificador = models.IntegerField(null=True)
    datos_payu = models.TextField()
    comprado = models.BooleanField(default=False)
    pendiente = models.BooleanField(default=False)
    fechaRealizado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    actualizado = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")
 
    class Meta:
        verbose_name = "Carrito de compras"
        verbose_name_plural = "Carritos de compras"
        ordering = ['-actualizado']

    @property
    def multi_ca_pre(self):
        mult = (self.precio * self.cantidad)
        return mult
    
    def save(self):
        self.total = self.multi_ca_pre
        super (CarritoCompras, self).save()

    def __str__(self):
        return "{} : {} - ${} - C: {} - ${} - id: {}".format(self.usuario, self.producto, self.precio, self.cantidad, self.total, self.identificador)