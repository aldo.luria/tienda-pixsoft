# Generated by Django 2.2.11 on 2020-04-03 06:39

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('productos', '0015_auto_20200403_0034'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='carritocompras',
            options={'ordering': ['-fechaRealizado'], 'verbose_name': 'Carrito de compras', 'verbose_name_plural': 'Carritos de compras'},
        ),
        migrations.AddField(
            model_name='carritocompras',
            name='cantidad',
            field=models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(1)]),
        ),
        migrations.AddField(
            model_name='carritocompras',
            name='fechaRealizado',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now, verbose_name='Fecha de creación'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='carritocompras',
            name='sucursal',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='carrito_sucursal', to='productos.Sucursal'),
            preserve_default=False,
        ),
    ]
