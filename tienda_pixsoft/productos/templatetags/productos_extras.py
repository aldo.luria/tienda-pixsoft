from django import template
from tienda_pixsoft.productos.models import Marca, Sucursal, Categoria

register = template.Library()

@register.simple_tag
def get_marca_list():
    marcas = Marca.objects.all()
    return marcas

@register.simple_tag
def get_sucursal_list():
    sucursales = Sucursal.objects.all()
    return sucursales   
    
@register.simple_tag
def get_categoria_list():
    categorias = Categoria.objects.all()
    return categorias  