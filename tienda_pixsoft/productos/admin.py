from django.contrib import admin
from .models import Categoria, Marca, Producto, Comentario, ImagenesProducto, Sucursal, CarritoCompras

@admin.register(Categoria, Marca, Producto, Comentario, ImagenesProducto, Sucursal, CarritoCompras)
class AuthorAdmin(admin.ModelAdmin):
    pass
