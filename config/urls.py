from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views import defaults as default_views

from tienda_pixsoft.users.views import (
    Indice, Ingresar, Salir, Registrar, CambiarPerfil, CambiarEmail,
    CrearCategoria, ListarCategoria, EditarCategoria, EliminarCategoria,
    CrearMarca, ListarMarca, EditarMarca, EliminarMarca,
    CrearSucursal, ListarSucursal, EditarSucursal, EliminarSucursal,
    ListarProductoAdministrador, ImagenProducto, ListarVenta,
    CrearProducto, ListarProducto, EditarProducto, EliminarProducto, DetalleProducto, 
    ComentarioProducto, AgregarImagen, EliminarImagen,
    AnadirCarrito, ListarCarrito, ListarCarritoPendientes, ListarCarritoFinalizadas,
    EliminarCarrito, SummaryView, DetailPaymentView, updateCar
    )

urlpatterns = [

    path('',Indice.as_view(),name='indice'),
   
    # Paths de usuario
    path('ingresar/',Ingresar.as_view(),name='ingresar'),
    path('salir/',Salir.as_view(),name='salir'),
    path('registrar/', Registrar.as_view(),name='registrar'),
    path('cambiar_perfil/',CambiarPerfil.as_view(),name='cambiar_perfil'),
    path('cambiar_perfil/email/',CambiarEmail.as_view(),name='cambiar_email'),

    # Paths de categoría categoria
    path('categoria/',ListarCategoria.as_view(),name='listar_categoria'),
    path('categoria/crear/',CrearCategoria.as_view(),name='crear_categoria'),
    path('categoria/editar/<int:pk>/',EditarCategoria.as_view(),name='editar_categoria'),
    path('categoria/eliminar/<int:pk>/',EliminarCategoria.as_view(),name='eliminar_categoria'),
        
    # Paths de marca
    path('marca/',ListarMarca.as_view(),name='listar_marca'),
    path('marca/crear/',CrearMarca.as_view(),name='crear_marca'),
    path('marca/editar/<int:pk>/',EditarMarca.as_view(),name='editar_marca'),
    path('marca/eliminar/<int:pk>/',EliminarMarca.as_view(),name='eliminar_marca'),

    # Paths de sucursal
    path('sucursal/',ListarSucursal.as_view(),name='listar_sucursal'),
    path('sucursal/crear/',CrearSucursal.as_view(),name='crear_sucursal'),
    path('sucursal/editar/<int:pk>/',EditarSucursal.as_view(),name='editar_sucursal'),
    path('sucursal/eliminar/<int:pk>/',EliminarSucursal.as_view(),name='eliminar_sucursal'),

    # Paths de producto
    path('producto/',ListarProducto.as_view(),name='listado_productos'),
    path('productos/',ListarProductoAdministrador.as_view(),name='listar_producto'),
    path('producto/imagenes/<int:pk>/',ImagenProducto.as_view(),name='imagen_admin_producto'),
    path('producto/detalle/<int:pk>/',DetalleProducto.as_view(),name='detalle_producto'),
    path('producto/crear/',CrearProducto.as_view(),name='crear_producto'),
    path('producto/editar/<int:pk>/',EditarProducto.as_view(),name='editar_producto'),
    path('producto/eliminar/<int:pk>/',EliminarProducto.as_view(),name='eliminar_producto'),

    path('agregar_imagen/', AgregarImagen.as_view(),name='agregar_imagen'),
    path('producto/editar/imagen/<int:pk>/', EliminarImagen.as_view(),name='eliminar_imagen'),

    # Paths de comentario
    path('crear_comentario/',ComentarioProducto.as_view(),name='crear_comentario'),

    # Paths de carrito
    path('anadir_carrito/',AnadirCarrito.as_view(),name='anadir_carrito'),
    path('listar_carrito/',ListarCarrito.as_view(),name='listar_carrito'),
    path('listar_pendientes/',ListarCarritoPendientes.as_view(),name='listar_pendientes'),
    path('listar_finalizado/',ListarCarritoFinalizadas.as_view(),name='listar_finalizado'),
    path('eliminar_carrito/<int:pk>/',EliminarCarrito.as_view(),name='eliminar_carrito'),

    path('listar_venta/',ListarVenta.as_view(),name='listar_venta'),

    # Paths de compras
    path('confirmacion/',SummaryView.as_view(),name='confirmation'),
    path('pagar/',DetailPaymentView.as_view(),name='pagar'),
    path('cambiar-carrito/',updateCar,name='actualizar_carrito'),


    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
